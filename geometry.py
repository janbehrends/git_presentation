import numpy as np

class NDim(object):
    def __init__(self):
        self.d = None
    def volume(self):
        return None
    def surface(self):
        return None

def Hypercube(NDim):
    def __init__(self,d,a):
        self.d = d
        self.a = a
    def volume(self):
        return self.a**self.d
    def surface(self):
        return 2*self.d*self.a**(self.d-1)

def NSphere(NDim):
    def __init__(self,d,r):
        self.d = d
        self.r = r
    def volume(self):
        vn = np.pi**(self.d/2.)/special.gamma(self.d/2.+1)
        return vn*self.r**self.d
    def surface(self):
        sn = 2*np.pi**((self.d+1)/2.)/special.gamma( (self.d+1)/2.)
        return sn*self.r**(self.d-1)

class TwoDim(NDim):
    def __init__(self):
        self.d = 2
    def area(self):
        return None
    def circumference(self):
        return None


class Rectangle(TwoDim):
    def __init__(self,d,d):
        self.a = d
        self.b = d
    def area(self):
        return self.a*self.b
    def circumference(self):
        return 2*(self.a + self.b)

class Circle(TwoDim):
    def __init__(self,r):
        self.r = r
    def area(self):
        return np.pi*self.r**2
    def circumference(self):
        return 2*np.pi*self.r

class RightTriangle(TwoDim):
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def area(self):
        return self.a*self.b/2.
    def circumference(self):
        c = np.sqrt(self.a**2 + self.b**2)
        return self.a+self.b+c

class Pentagon(TwoDim):
    def __init__(self,a):
        self.a = a
    def area(self):
        tmp = np.sqrt(5*(5+2*np.sqrt(5)))
        return tmp/4.*self.a**2

class ThreeDim(object):
    def __init__(self):
        self.d = 2
    def volume(self):
        return None
    def surface(self):
        return None

class Sphere(ThreeDim):
    def __init__(self,r):
        self.r = r
    def volume(self):
        return 4*np.pi/3*self.r**3
    def surface(self):
        return 4*np.pi*self.r**2

class Cube(ThreeDim):
    def __init__(self,a):
        self.a = a
    def volume(self):
        return self.a**3
    def surface(self):
        return 6*self.a**2

class Pyramid(ThreeDim):
    def __init__(self,a,h):
        self.a = a
        self.h = h
    def volume(self):
        return self.h*self.a**2/3.
    def surface(self):
        return self.a**2 + 2*self.a*self.h
