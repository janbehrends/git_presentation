# Tutorial: git

This is supposed to be a short hands-on exercise on git. We will have the following examples

- Setting up a git directory
- Commit, push, pull
- Conflicts
- Working together
- Branches

### Why git?

Better version control of your software:

- keep track of changes of your code
- work together with your coworkers on the same project at the same time
- use branches to introduce new features instead of messing with your nicely working script.

It's popular. Wikipedia knows: [in May 2014], 42.9% of professional software developers reported that they use Git as their primary source control system.

## Setting up a git directory

It is convenient to use a web-based host for git. In principle, this is not necessary and one can also set up their own repository either on a local machine or web-based (dropbox, owncloud).
Here, we will focus on a web-based host. Examples include
- bitbucket
- gitlab
- github

Depending on the site, private or public repositories are allowed. On your local machine, an *index*, a *stash*  and a *local repository* are stored besides what you see in a folder called '.git'. **Never** change anything there.

First initialize folder and connect it with the repository

    git init
    git remote add origin [origin]

or, if you already set up things in a web-based repository

    git clone [origin]

Here, please type

    git clone git@bitbucket.org:janbehrends/git_presentation.git

You will find some files (including this README) and, as described, a folder called '.git'.

## Commit, push, pull

After adding or changing a file within your *workspace*, type

    git status

which will give a list of files and show the files
- within your *workspace*,
- already added to the *index*, and
- if there are differences between the workspace and the *local repository*.
Files can be added to the *index* via

    git add [filename]

and then commit them to the *local repository* via

    git commit
    git commit -m [message]

If there are no new files to add and you just want to commit your changes, use the abbreviation

    git commit -a
    git commit -am [message]

Finally, the changes must be pushed from the *local repository* to the *remote repository* via

    git push [origin] [branch]

where we will use

    git push origin master

here. Changes in the *remote repository* can be integrated to your workspace via

    git pull [origin] [branch]

An overview including more commands is given here:

![Alt](/git-transport.png "Workflow with git")

## Working together with your buddies

Join the bitbucket folder and we can work together on the same project.

## Branches

New branches are first created via

    git branch [branch_name]

We then switch to the new branch

    git checkout [branch_name]

Both operations in one command

    git checkout -b [branch_name]

A list of available branches is created via

    git branch

Delete branch

    git branch -d [branch_name]


